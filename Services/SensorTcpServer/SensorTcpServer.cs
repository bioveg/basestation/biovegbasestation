﻿using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;
using BiovegBasestation.Services.Logger;
using BiovegBasestation.Services.SensorHubService;
using BiovegBasestation.Utils.SensorUtils;

namespace BiovegBasestation.Services.SensorTcpServer
{
    public class SensorTcpServer : ITcpServer

    {
        private TcpListener tcpListener;
        private ISensorHubService sensorHubService;
        private ISensorDataSerializer sensorDataSerializer;
        private ILogger logger;
        private CancellationTokenSource tokenSource;

        public SensorTcpServer()
        {
        }

        public SensorTcpServer(TcpListener tcpListener, ISensorHubService sensorHubService,
            ISensorDataSerializer sensorDataSerializer, ILogger logger)
        {
            this.tcpListener = tcpListener;
            this.sensorHubService = sensorHubService;
            this.sensorDataSerializer = sensorDataSerializer;
            this.logger = logger;
        }

        public async Task StartAsync(CancellationToken token)
        {
            try
            {
                this.tokenSource = CancellationTokenSource.CreateLinkedTokenSource(token);
                this.tcpListener.Start();
                ConsoleOut($"Listening...");

                while (!token.IsCancellationRequested)
                {
                    var client = await this.tcpListener.AcceptTcpClientAsync();
                    _ = HandleClientAsync(client);
                }
            }
            catch (SocketException e)
            {
                ConsoleOut(e.Message);
            }
        }

        public void Stop() => this.tcpListener.Stop();
        public void Cancel() => this.tokenSource.Cancel();
        public void WithLogging(ILogger logger) => this.logger = logger;
        public void WithListener(TcpListener listener) => this.tcpListener = listener;
        public void WithHubService(ISensorHubService sensorHubService) => this.sensorHubService = sensorHubService;

        public void WithSerializer(ISensorDataSerializer sensorDataSerializer) =>
            this.sensorDataSerializer = sensorDataSerializer;

        private async Task HandleClientAsync(TcpClient client)
        {
            try
            {
                await using var stream = client.GetStream();
                using var sr = new StreamReader(stream);
                var data = await sr.ReadToEndAsync();
                var sensorEndPoint = (IPEndPoint) client.Client.RemoteEndPoint;
                var sensorIp = sensorEndPoint.Address;
                var sensor = this.sensorDataSerializer.Serialize($"{sensorIp},{data}");
                ConsoleOut($"Sensor IP: {sensorIp} Data: {data}");

                if (this.sensorHubService.Connected())
                    await this.sensorHubService.DispatchAsync(sensor);

                client.Dispose();
            }
            catch (Exception e)
            {
                ConsoleOut(e.Message);
            }
        }

        private void ConsoleOut(string message) => logger?.ConsoleOut($"[TCP Server: Sensor] {message}");
    }
}