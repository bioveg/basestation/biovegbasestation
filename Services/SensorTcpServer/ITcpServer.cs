﻿using System.Threading;
using System.Threading.Tasks;

namespace BiovegBasestation.Services.SensorTcpServer
{
    public interface ITcpServer
    {
        Task StartAsync(CancellationToken token);
        void Stop();
        void Cancel();
    }
}