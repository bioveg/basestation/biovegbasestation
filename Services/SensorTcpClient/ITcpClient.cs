﻿using System.Threading.Tasks;

namespace BiovegBasestation.Services.SensorTcpClient
{
    public interface ITcpClient
    {
        Task Response(string data);
    }
}