﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using BiovegBasestation.Services.Logger;

namespace BiovegBasestation.Services.SensorTcpClient
{
    public class SensorTcpClient : ITcpClient
    {
        private readonly TcpClient tcpClient;
        private readonly ILogger logger;

        public SensorTcpClient(TcpClient tcpClient, ILogger logger)
        {
            this.tcpClient = tcpClient;
            this.logger = logger;
        }

        public async Task Response(string data)
        {
            var ipNotValid = !IPAddress.TryParse(data, out var ipAddress);
            if (ipNotValid) throw new Exception($"'{nameof(data)}' is not an IP address.");
            await this.tcpClient.ConnectAsync(ipAddress, 9999);
            var stream = this.tcpClient.GetStream();
            var sensorData = Encoding.ASCII.GetBytes(data);
            await stream.WriteAsync(sensorData, 0, sensorData.Length);
            ConsoleOut($"Response from server: {data}");
        }

        private void ConsoleOut(string message) => this.logger.ConsoleOut($"[TCP Client] {message}");
    }
}