﻿using System.Threading.Tasks;
using BiovegBasestation.Models.Sensor;

namespace BiovegBasestation.Services.SensorHubService
{
    public interface ISensorHubService
    {
        bool Connected();
        Task DispatchAsync(SensorData data);
    }
}