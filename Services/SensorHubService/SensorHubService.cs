﻿using System;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using BiovegBasestation.Models.Sensor;
using BiovegBasestation.Services.Logger;
using BiovegBasestation.Services.SensorTcpClient;
using Microsoft.AspNetCore.SignalR.Client;

namespace BiovegBasestation.Services.SensorHubService
{
    public class SensorHubService : ISensorHubService
    {
        private readonly ITcpClient tcpClient;
        private readonly HubConnection hubConnection;
        private readonly ILogger logger;

        public SensorHubService(HubConnection hubConnection, ITcpClient tcpClient, ILogger logger)
        {
            this.hubConnection = hubConnection;
            this.tcpClient = tcpClient;
            this.logger = logger;
        }

        public bool Connected() => this.hubConnection.State == HubConnectionState.Connected;

        public async Task DispatchAsync(SensorData sensorData)
        {
            ConsoleOut($"Dispatching: [{sensorData.Id}],[{sensorData.Type}],[{sensorData.Value}]");
            await this.hubConnection.InvokeAsync("Dispatch", sensorData);
        }

        public async Task StartAsync()
        {
            this.hubConnection.Closed += ClosedAsync;
            this.hubConnection.On<string>("ReceiveMessage", this.tcpClient.Response);
            await ConnectAsync();
        }

        private async Task ConnectAsync()
        {
            ConsoleOut($"Awaiting hub response...");
            await this.hubConnection.StartAsync();
            ConsoleOut($"Connected! (ID: {this.hubConnection.ConnectionId})");
        }

        private async Task ClosedAsync(Exception e)
        {
            ConsoleOut($"Connection to {this.hubConnection.ConnectionId} closed!\nReason: {e.Message}");
            await ConnectAsync();
        }

        private void ConsoleOut(string message) => this.logger?.ConsoleOut($"[Hub Client: Sensor] {message}");
    }
}