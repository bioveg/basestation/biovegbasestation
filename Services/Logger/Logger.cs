﻿using System;

namespace BiovegBasestation.Services.Logger
{
    public class Logger : ILogger
    {
        public void ConsoleOut(string message)
        {
            Console.WriteLine(message);
        }

        public void Log(string message)
        {
            // Log persistant
        }
    }
}