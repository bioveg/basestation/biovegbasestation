﻿using System.Net;

namespace BiovegBasestation.Startup
{
    public interface ILaunchArgsContainer
    {
        public IPAddress IpAddress { get; }
        public int Port { get; }
    }
}