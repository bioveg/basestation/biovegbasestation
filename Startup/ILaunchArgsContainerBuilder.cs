﻿using System.Net;

namespace BiovegBasestation.Startup
{
    public interface ILaunchArgsContainerBuilder
    {
       ILaunchArgsContainerBuilder WithIpAddress(IPAddress ipAddress);
       ILaunchArgsContainerBuilder WithPort(int port);
       ILaunchArgsContainer Build();
    }
}