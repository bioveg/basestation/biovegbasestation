﻿using System.Net;

namespace BiovegBasestation.Startup
{
    public class LaunchArgsContainer : ILaunchArgsContainer
    {
        public IPAddress IpAddress { get; private set; }
        public int Port { get; private set; }
        public void WithIpAddress(IPAddress ipAddress) => this.IpAddress = ipAddress;
        public void WithPort(int port) => this.Port = port;
  
    }
}