﻿using System.Net;

namespace BiovegBasestation.Startup
{
    public class LaunchArgsContainerBuilder : ILaunchArgsContainerBuilder
    {
        private readonly LaunchArgsContainer launchArgsContainer = new();

        public ILaunchArgsContainerBuilder WithIpAddress(IPAddress ipAddress)
        {
            this.launchArgsContainer.WithIpAddress(ipAddress);
            return this;
        }

        public ILaunchArgsContainerBuilder WithPort(int port)
        {
            this.launchArgsContainer.WithPort(port);
            return this;
        }

        public ILaunchArgsContainer Build() => this.launchArgsContainer;
    }
}