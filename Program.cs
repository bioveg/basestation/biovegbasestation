﻿using System;
using System.Net.Sockets;
using System.Threading;
using BiovegBasestation.Services.Logger;
using BiovegBasestation.Services.SensorHubService;
using BiovegBasestation.Services.SensorTcpClient;
using BiovegBasestation.Utils.SensorTcpServerUtils;
using BiovegBasestation.Utils.SensorUtils;
using Microsoft.AspNetCore.SignalR.Client;

namespace BiovegBasestation
{
    class Program
    {
        private static void Main(string[] args)
        {
            // Launch Args Container for easy
            var launchArgsContainer = Settings.HandleLaunchArgs(args);

            // Logger
            ILogger logger = new Logger();
            PrintWelcome(logger);

            // Sensor data serializer
            ISensorDataDeserializer sensorDataDeserializer = new SensorDataDeserializer();
            ISensorDataSerializer sensorDataSerializer = new SensorDataSerializer();

            // TCP Client (for sending data to sensors)
            var sensorTcpClient = new SensorTcpClient(new TcpClient(), logger);

            // SignalR Hub Client (for connecting with backend)
            var hubConnection = new HubConnectionBuilder().WithUrl(Settings.BackendUrl).Build();
            var sensorHubService = new SensorHubService(hubConnection, sensorTcpClient, logger);
            _ = sensorHubService.StartAsync();

            // TCP server (for receiving data from sensors)
            var tcpListener = new TcpListener(launchArgsContainer.IpAddress, launchArgsContainer.Port);
            var sensorTcpServer = new SensorTcpServerBuilder()
                .WithSerializer(sensorDataSerializer)
                .WithSensorHubService(sensorHubService)
                .WithTcpListener(tcpListener)
                .WithLogging(logger).Build();
            var cancellationToken = new CancellationToken();
            _ = sensorTcpServer.StartAsync(cancellationToken);

            StayAlive(logger);
        }

        private static void StayAlive(ILogger logger)
        {
            while (true)
            {
                string userSentence = Console.ReadLine()?.ToLower();
                if (userSentence == Settings.TerminationWord) break;
            }

            logger.ConsoleOut("Terminating application...");
        }

        private static void PrintWelcome(ILogger logger)
        {
            logger.ConsoleOut($"{Settings.AppName} V{Settings.Version}");
            logger.ConsoleOut($"Type '{Settings.TerminationWord}' to shut down the application.");
        }
    }
}