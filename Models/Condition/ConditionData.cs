﻿namespace BiovegBasestation.Models.Condition
{
    public class ConditionData
    {
        public ConditionData(string name, int min, int max)
        {
            this.Name = name;
            this.Min = min;
            this.Max = max;
        }

        public string Name { get; private set; }
        public int Min { get; private set; }
        public int Max { get; private set; }

        public bool IsConditionOptimal(int value) => value > Min && value < Max;
    }
}