﻿using System;

namespace BiovegBasestation.Models.Sensor
{
    public class SensorData 
    {
        public SensorData(string id, int type, float value, DateTime dateTime)
        {
            this.Id = id;
            this.Type = type;
            this.Value = value;
        }

        public string Id { get; }
        public int Type { get; }
        public float Value { get; }
    }
}