﻿using System.Net;
using BiovegBasestation.Startup;

namespace BiovegBasestation
{
    public static class Settings
    {
        public const string BackendUrl = "http://wgniash.duckdns.org:15000/BaseStationHub";
        public const string TerminationWord = "kill";
        public const string Version = "0.1";
        public const string AppName = "Bioveg Basestation";

        public static ILaunchArgsContainer HandleLaunchArgs(string[] args)
        {
            bool argsPassed = args.Length > 0;
            bool argsPassedPort = args.Length > 1;
            var ipAddress = IPAddress.Parse(argsPassed ? args[0] : IPAddress.Any.MapToIPv4().ToString());
            ushort port = argsPassedPort ? ushort.Parse(args[1]) : (ushort) 13000;

            return new LaunchArgsContainerBuilder().WithIpAddress(ipAddress).WithPort(port).Build();
        }
    }
}