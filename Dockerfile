# syntax=docker/dockerfile:1
FROM mcr.microsoft.com/dotnet/sdk:5.0-buster-slim-arm64v8 AS build-env
#FROM mcr.microsoft.com/dotnet/sdk:5.0.202-buster-slim-arm64v8 AS build-env
WORKDIR /app

# Copy csproj and restore as distinct layers
COPY *.csproj ./
RUN dotnet restore

# Copy everything else and build
COPY . ./
RUN dotnet publish -c Release -o out

# Build runtime image
FROM mcr.microsoft.com/dotnet/runtime:3.1.15-buster-slim-arm64v8
WORKDIR /app
COPY --from=build-env /app/out .
ENTRYPOINT ["dotnet", "BiovegBasestation.dll"]
