﻿using System.Net.Sockets;
using BiovegBasestation.Services.Logger;
using BiovegBasestation.Services.SensorHubService;
using BiovegBasestation.Services.SensorTcpServer;
using BiovegBasestation.Utils.SensorUtils;

namespace BiovegBasestation.Utils.SensorTcpServerUtils
{
    public interface ISensorTcpServerBuilder
    {
        ISensorTcpServerBuilder WithLogging(ILogger logger);
        ISensorTcpServerBuilder WithTcpListener(TcpListener listener);
        ISensorTcpServerBuilder WithSensorHubService(ISensorHubService sensorHubService);
        ISensorTcpServerBuilder WithSerializer(ISensorDataSerializer sensorDataSerializer);
        ITcpServer Build();
    }
}