﻿using System.Net.Sockets;
using BiovegBasestation.Services.Logger;
using BiovegBasestation.Services.SensorHubService;
using BiovegBasestation.Services.SensorTcpServer;
using BiovegBasestation.Utils.SensorUtils;

namespace BiovegBasestation.Utils.SensorTcpServerUtils
{
    public class SensorTcpServerBuilder : ISensorTcpServerBuilder
    {
        private readonly SensorTcpServer sensorTcpServer = new SensorTcpServer();

        public ISensorTcpServerBuilder WithLogging(ILogger logger)
        {
            this.sensorTcpServer.WithLogging(logger);
            return this;
        }
        
        public ISensorTcpServerBuilder WithTcpListener(TcpListener listener)
        {
            this.sensorTcpServer.WithListener(listener);
            return this;
        }
        
        public ISensorTcpServerBuilder WithSensorHubService(ISensorHubService sensorHubService)
        {
            this.sensorTcpServer.WithHubService(sensorHubService);
            return this;
        }
        
        public ISensorTcpServerBuilder WithSerializer(ISensorDataSerializer sensorDataSerializer)
        {
            this.sensorTcpServer.WithSerializer(sensorDataSerializer);
            return this;
        }

        public ITcpServer Build() => this.sensorTcpServer;
    }
}