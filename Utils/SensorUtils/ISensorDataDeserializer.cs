﻿using BiovegBasestation.Models.Sensor;

namespace BiovegBasestation.Utils.SensorUtils
{
    public interface ISensorDataDeserializer
    {
        string Deserialize(SensorData sensorData);
    }
}