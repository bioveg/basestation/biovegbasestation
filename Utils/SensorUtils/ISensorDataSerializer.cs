﻿using BiovegBasestation.Models.Sensor;

namespace BiovegBasestation.Utils.SensorUtils
{
    public interface ISensorDataSerializer
    {
        SensorData Serialize(string data);
    }
}