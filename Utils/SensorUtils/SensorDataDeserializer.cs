﻿using BiovegBasestation.Models.Sensor;

namespace BiovegBasestation.Utils.SensorUtils
{
    public class SensorDataDeserializer : ISensorDataDeserializer
    {
        public string Deserialize(SensorData sensorData) => $"{sensorData.Id},{sensorData.Type},{sensorData.Value}";
    }
}