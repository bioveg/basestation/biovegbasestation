﻿using System;
using System.Runtime.Serialization;
using BiovegBasestation.Models.Sensor;
using BiovegBasestation.Services.Logger;

namespace BiovegBasestation.Utils.SensorUtils
{
    public class SensorDataSerializer : ISensorDataSerializer
    {
        public SensorData Serialize(string sensorData)
        {
            if (sensorData == null) throw new NullReferenceException($"Object '{nameof(sensorData)}' is null.");
            string[] values = sensorData.Split(',');
            int props = typeof(SensorData).GetProperties().Length;
            if (values.Length != props)
                throw new SerializationException(
                    $"Unable to serialize data to model [{nameof(SensorData)}]. Data received: [{sensorData}] Expected: [string,int,float]");
            string id = values[0];
            int type = int.Parse(values[1]);
            float value = float.Parse(values[2]);

            return new SensorData(id, type, value, DateTime.Now);
        }
    }
}